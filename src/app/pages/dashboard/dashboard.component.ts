import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  selectedUsagePeriod = 'WEEK';

  charts = {
    usageChart: {
      chartLabels: ['8th June', '9th June', '10th June', '11th June', '12th June', '13th June', '14th June'],
      chartData: [
        { data: [50, 75, 32, 44, 26, 50, 111], label: 'Minutes' }
      ],
      chartOptions: {
        responsive: true,
        scales: {
          yAxes: [{
            gridLines: {
              color: 'rgba(0, 0, 0, 0)',
            }
          }]
        },
        plugins: {
          datalabels: {
            anchor: 'end',
            align: 'end',
          }
        }
      },
    }
  };

  chartPlugins = [pluginDataLabels];

  constructor() { }

  ngOnInit(): void {
  }

}
