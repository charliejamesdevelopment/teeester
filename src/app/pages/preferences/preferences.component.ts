import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {createPreferencesFormGroup, createSettingFormGroup} from './form-group/preferences.form-group';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.scss']
})
export class PreferencesComponent implements OnInit {

  entityForm: FormGroup;
  allSettingsFormArray: FormArray;

  titleOptions = ['Mr', 'Mrs', 'Miss', 'Mx'];
  settingsArray = [
    {
      id: 1,
      settingDescription: 'Setting description for 1',
      value: false
    },
    {
      id: 2,
      settingDescription: 'Setting description for 2',
      value: false
    },
    {
      id: 3,
      settingDescription: 'Setting description for 3',
      value: true
    },
    {
      id: 4,
      settingDescription: 'Setting description for 4',
      value: false
    },
    {
      id: 4,
      settingDescription: 'Fill these in when we have some!',
      value: true
    }
  ];

  constructor(private formBuilder: FormBuilder) {
    this.entityForm = createPreferencesFormGroup(formBuilder);
    this.allSettingsFormArray = (this.entityForm.get('settings') as FormArray);
  }

  ngOnInit(): void {
    this.updateSettings(this.settingsArray);
  }

  save(): void {}

  updateSettings(settings: any): void {
    this.allSettingsFormArray.controls.splice(0);

    for (const setting of this.settingsArray) {
      if (setting) {
        const fg = createSettingFormGroup(this.formBuilder);

        fg.patchValue(setting);
        this.allSettingsFormArray.push(fg);
      }
    }
  }

}
