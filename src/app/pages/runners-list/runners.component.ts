import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-runners',
  templateUrl: './runners.component.html',
  styleUrls: ['./runners.component.scss']
})
export class RunnersComponent implements OnInit {

  items = [{ title: 'Edit' }, { title: 'Delete' }];

  constructor() { }

  ngOnInit(): void {
  }

}
