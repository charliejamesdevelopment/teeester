import {NbMenuItem} from '@nebular/theme';

export const userNavigation = [
  {
    name: 'Main',
    items: [
      {
        title: 'Dashboard',
        icon: 'pie-chart-outline',
        link: '/'
      },
      {
        title: 'Runners',
        icon: 'activity-outline',
        link: '/runners'
      },
    ]
  },
  {
    name: 'Account',
    items: [
      {
        title: 'Preferences',
        icon: 'settings-2-outline',
        link: '/preferences'
      },
      {
        title: 'Billing',
        icon: 'credit-card-outline',
      },
      {
        title: 'Logout',
        icon: 'unlock-outline',
      },
    ]
  }
]

