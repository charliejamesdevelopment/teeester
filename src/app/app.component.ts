import { Component } from '@angular/core';
import {userNavigation} from './navigation/navigation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';
  navigationItems = userNavigation;
}
