import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {AbstractControl, ControlContainer, FormControl, FormGroup} from '@angular/forms';

@Component({
  template: ''
})
export class FormElementComponent implements OnInit{
  @Input() field: string;

  public form: FormGroup;
  public control: FormControl;

  error = false;

  constructor(private controlContainer: ControlContainer) {}

  // Might need this later. Keeping incase its useful
  clear(): void {
    this.control.reset();
  }

  ngOnInit(): void {
    this.form = (this.controlContainer.control as FormGroup);
    if (this.form && this.field) {
      this.control = (this.form.get(this.field) as FormControl);
      this.subscribeToFormGroup();
    }
  }

  subscribeToFormGroup(): void {
    if (this.form) {
      this.control.valueChanges.subscribe(() => {
        this.error = !this.control.valid;
      });
    }
  }

  formControlMandatory(): boolean {
    if (!this.control) {
      return false;
    }

    if (this.control.validator) {
      const validator = this.control.validator({} as AbstractControl);
      if (validator && validator.required) {
        return true;
      }
    }

    return false;
  }
}
