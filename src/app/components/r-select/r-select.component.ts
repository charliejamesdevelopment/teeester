import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormElementComponent} from '../form-element.component';
import {ControlContainer} from '@angular/forms';

@Component({
  selector: 'app-r-select',
  templateUrl: './r-select.component.html'
})
export class RSelectComponent extends FormElementComponent implements OnInit {

  @Input() label: string;
  @Input() disabled = false;
  @Input() options = [];
  @Input() labelAttribute: string;

  @Output() change: EventEmitter<any> = new EventEmitter<any>();

  constructor(controlContainer: ControlContainer) {
    super(controlContainer);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

}
