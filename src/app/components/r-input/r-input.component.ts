import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ControlContainer} from '@angular/forms';
import {FormElementComponent} from '../form-element.component';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-r-input',
  templateUrl: './r-input.component.html'
})
export class RInputComponent extends FormElementComponent implements OnInit {

  @Input() label: string;
  @Input() icon: string;
  @Input() disabled = false;

  @Output() change: EventEmitter<any> = new EventEmitter<any>();

  constructor(controlContainer: ControlContainer) {
    super(controlContainer);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

}
