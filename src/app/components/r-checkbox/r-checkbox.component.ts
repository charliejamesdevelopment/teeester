import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormElementComponent} from '../form-element.component';
import {ControlContainer} from '@angular/forms';

@Component({
  selector: 'app-r-checkbox',
  templateUrl: './r-checkbox.component.html'
})
export class RCheckboxComponent extends FormElementComponent implements OnInit {

  @Input() label: string;
  @Input() disabled = false;

  value = false;

  @Output() change: EventEmitter<any> = new EventEmitter<any>();

  constructor(controlContainer: ControlContainer) {
    super(controlContainer);
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (this.control.value) {
      this.value = this.control.value;
    }
  }

  checkedChange(value: boolean): void {
    this.value = value;

    this.control.patchValue(value);
    this.change.emit(value);
  }

  /*subscribeToFormGroup(): void {
    if (this.form) {
      this.control.valueChanges.subscribe((value) => {
        this.error = !this.control.valid;


      });
    }
  }*/

}
